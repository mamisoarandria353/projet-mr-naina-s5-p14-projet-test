/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import annotation.UrlAnnotation;
import model.ModelView;

/**
 *
 * @author 26132
 */
public class Olona {

    private String id;
    private String nom;
    private int age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Olona(String id, String nom, int age) {
        this.id = id;
        this.nom = nom;
        this.age = age;
    }

    public Olona() {
    }

    @UrlAnnotation(url = "Select")
    public ModelView Select() {
        ModelView md = new ModelView();
        Olona[] liste = new Olona[2];
        liste[0] = new Olona("Olona_1", "Rabe", 41);
        liste[1] = new Olona("Olona_2", "Randria", 30);

        md.setAttribute("ListeOlona", liste);
        md.setUrl("ListeOlona.jsp");
        return md;
    }

    @UrlAnnotation(url = "Insert")
    public void Insert() {
        
        System.out.println(this.id);
        System.out.println(this.nom);
    }
}
